--- 

#### 版本: 1.0.0.113-SNAPSHOT
* 时间: 2019-03-08 17:20:34
#### 更新点
1. 
2. 
3. 
#### Maven:
``` 
com.bftv.fui:lib2:1.0.0.113-SNAPSHOT
``` 

#### 最后提交:
``` 
commit 24c8fc66c9b08b0eb748205458f2183781527942
Author: zhaopeng <zhaopeng@bftv.com>
Date:   2019-03-08 16:56:08 +0800

    打包发送邮件时添加默认的css.
``` 
