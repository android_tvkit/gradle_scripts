--- 

#### 版本: 1
* 时间: 2019-10-24 11:41:55
* [参考文档](https://gitlab.fengmi.tv/tv-public/gradle_project/blob/master/README.md)
#### 更新点
* build_publish_lib_basic.gradle添加 Task: publishSupportLibSnapshotNoMail.在上传snapshot版本时，不发送邮件通知
(publishSupportLibSnapshot默认发送，不配置邮箱的情况下，使用ott_dev_info)。

#### [README](https://gitlab.com/android_tvkit/gradle_scripts/blob/master/README.md) [CHANGELOG](https://gitlab.com/android_tvkit/gradle_scripts/blob/master/CHANGELOG.md)
#### Maven:
``` 
huan.tvkit:gradle-scripts:1
``` 

#### 最后提交:
``` 
commit 5f10cdf8879f3e99243dac431e4e8e54c66c90cd
Author: zhaopeng <zhaopeng511@gmail.com>
Date:   Wed Oct 23 20:36:02 2019 +0800

    更新Readme
``` 
