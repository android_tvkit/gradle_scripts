--- 

#### 版本: 1.0.2.12
* 时间: 2019-03-07 19:54:57
* [参考文档](https://gitlab.fengmi.tv/tv-public/gradle_project/blob/master/README.md)
#### 更新点
1. 优化发布流程，在目标project目录下生成publish目录，作为版本发布的工作目录。
2. 在publish目录下生成release和snapshot俩个文件夹，分别存放每次发版后的日志。
3. 添加版本发布时通过编写markdown文件来编写"更新点"的功能。
4. 添加task:mailPreview。用来预览邮件发送的内容。
5. 添加task:libProjectInit用来创建一个新的库项目发布所需要的必要文件。
6. 添加task:generatePublishRootDir生成publish目录的内容。并将文件添加至git.
7. 去除updatePublishLog*等任务

**注意，请将插件库更新至最新版本：**
```java
    com.bftv.tools.build:gradle:1.0.2
```
#### Maven:
``` 
com.bftv.fui:gradle-scripts:1.0.2.12
``` 

#### 最后提交:
``` 
commit fc39f69ff20adcf9348bc2f67793013b11d253bd
Author: zhaopeng <zhaopeng@bftv.com>
Date:   2019-01-15 15:37:53 +0800

    修正地址 引用 错误
``` 
