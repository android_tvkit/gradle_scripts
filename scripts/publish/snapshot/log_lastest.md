--- 

#### 版本: 1.0.0
* 时间: 2019-10-24 12:45:33
#### 更新点
* build_publish_lib_basic.gradle添加 Task: publishSupportLibSnapshotNoMail.在上传snapshot版本时，
不发送邮件通知(publishSupportLibSnapshot默认发送，不配置邮箱的情况下，使用ott_dev_info)。

* [README](https://gitlab.com/android_tvkit/gradle_scripts/blob/master/README.md) 
* [CHANGELOG](https://gitlab.com/android_tvkit/gradle_scripts/blob/master/CHANGELOG.md)
#### Maven:
``` 
huan.tvkit:gradle-scripts:1.0.0
``` 

#### 最后提交:
``` 
commit 70afafa17df6c193fa99ca7252eaadbad637f4f2
Author: zhaopeng <zhaopeng511@gmail.com>
Date:   Thu Oct 24 12:45:02 2019 +0800

    build_publish_lib_basic.gradle添加task : publishSupportLibSnapshotNoMail
``` 
